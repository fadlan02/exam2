package com.nexsoft;

import java.util.Locale;
import java.time.LocalDateTime;
import com.nexsoft.*;

/**
 * Hello world!
 *
 */
public class App extends SmartParking {

    public static void main(String[] args) {
        SmartParking kendaraan1 = new SmartParking();
        kendaraan1.setParkingId("MTR1h896");
        kendaraan1.kendaraanMotor("motor");
        kendaraan1.setJam(2);
        kendaraan1.setType("out");
        kendaraan1.hitungPembayaranMotor();
        kendaraan1.displayMotor();

        SmartParking kendaraan2 = new SmartParking();
        kendaraan2.setParkingId("MBL73275");
        kendaraan2.kendaraanMobil("Mobil");
        kendaraan2.setJam(5);
        kendaraan2.setType("out");
        kendaraan2.hitungPembayaranMobil();
        kendaraan2.displayMobil();

        SmartParking kendaraan3 = new SmartParking();
        kendaraan2.setParkingId("MBL42275");
        kendaraan2.kendaraanMobil("Mobil");
        kendaraan2.setJam(5);
        kendaraan2.setType("out");
        kendaraan2.hitungPembayaranMobil();
        kendaraan2.displayMobil();

    }
}
