package com.nexsoft;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import com.nexsoft.Vehicle;

public class SmartParking {
    String ParkingId;
    String kendaraanMotor;
    String kendaraanMobil;
    String Type;
    int PriceMobil = 5000;
    int PriceMotor = 3000;
    int jam;
    LocalDateTime in = LocalDateTime.now();
    LocalDateTime out = LocalDateTime.of(2020, 06, 23, 20, 10, 11);

    void setParkingId(String parkingId) {
        this.ParkingId = parkingId;
    }

    void kendaraanMotor(String kendaraanMotor) {
        this.kendaraanMotor = kendaraanMotor;
    }

    void kendaraanMobil(String kendaraanMobil) {
        this.kendaraanMobil = kendaraanMobil;
    }

    void setType(String Type) {
        this.Type = Type;
    }

    void setJam(int jam) {
        this.jam = jam;
    }

    public String getParkingId() {
        return ParkingId;
    }

    public int hitungPembayaranMotor() {
        return PriceMotor - 1000 + jam * 1000;
    }

    public int hitungPembayaranMobil() {
        return PriceMobil - 2000 + jam * 2000;
    }

    void displayMotor() {
        System.out.println("\n======================================\n");
        System.out.println("ID parkir \t: " + ParkingId);
        System.out.println("Kendaraan \t: " + kendaraanMotor);
        System.out.println("jam \t\t: " + jam);
        System.out.println("Waktu waksuk \t: " + in);
        System.out.println("Waktu Keluar \t: " + out);
        System.out.println("tipe  \t\t: " + Type);
        System.out.println("tarif \t\t: " + hitungPembayaranMotor());
        System.out.println("\n======================================\n");
    }

    void displayMobil() {

        System.out.println("ID parkir \t: " + ParkingId);
        System.out.println("Kendaraan \t: " + kendaraanMobil);
        System.out.println("jam \t\t: " + jam);
        System.out.println("Waktu waksuk \t: " + in);
        System.out.println("Waktu Keluar \t: " + out);
        System.out.println("tipe  \t\t: " + Type);
        System.out.println("tarif \t\t: " + hitungPembayaranMobil());
        System.out.println("\n======================================\n");
    }

}