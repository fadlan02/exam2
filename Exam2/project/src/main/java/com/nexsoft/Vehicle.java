package com.nexsoft;

public class Vehicle {
    public String motor;
    public String mobil;

    Vehicle(String motor, String mobil) {
        this.motor = motor;
        this.mobil = mobil;
    }

    void setKendaraanMotor(String motor) {
        this.motor = motor;
    }

    void setKendaraanMobil(String mobil) {
        this.mobil = mobil;
    }
}